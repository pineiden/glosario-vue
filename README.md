Glosario
--------

Esta app presenta un glosario agrupado por número-letra y con las
herramientas de búsqueda por símbolo utilizando el teclado, así como
también búsqueda por concepto o definición.

Permite conectarse a una API que contenga definiciones y buscar sobre ellas.

Teclado Alfanumérico
====================

https://gitlab.com/Raquelbracho77/flex-glosary

