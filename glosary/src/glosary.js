import axios from 'axios';
import VueAxios from 'vue-axios';
import * as components from './components';

const ComponentLibrary = {
    install(instance, options={}) {
        for (const componentName in components) {
            const component = components[componentName];
            //console.log("instance:: component name->", component.name)
            instance.component(component.name, component);            
        }
        instance.use(VueAxios, axios);
        instance.provide(
            'axios', 
            instance.config.globalProperties.axios);	
        instance.opts = options;
        instance.config.productionTip = false;
    }
};

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(ComponentLibrary);
}

export default ComponentLibrary;

