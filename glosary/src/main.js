import * as Vue from 'vue'
import ComponentLibrary from './glosary'
import App from './App.vue';

const app = Vue.createApp(App);
app.use(ComponentLibrary);
app.mount('#app');

